$(document).ready(function() {
    let app = new Vue({
        el: '#app',
        data: {
            steps: {},
            test: {},
            picked: '',
            stepId: 0,
            answer: false,
            config: false,
            trees: {},
            levels: {},
            treeId: 0,
            experience: 0
        },
        methods:{
            getSteps: function () {
                this.$http.post("./api/requestApi.php",{
                    method: "getSteps",
                }).then(function (data) {
                    this.steps = data.data;
                })
            },
            getTest: function (stepId, testId, levelId) {
                this.answer = false;
                this.picked = "";
                this.stepId = stepId;
                this.$http.post("./api/requestApi.php",{
                    method: "getTest",
                    testId: testId,
                    levelId: levelId
                }).then(function (data) {
                    this.test = data.data;
                })
            },
            sendAnswer: function () {
                if (this.picked !== '')
                {
                    $('#myModal').modal('toggle');
                    $("#" + this.stepId)
                        .removeClass("btn-success")
                        .addClass("btn-secondary")
                        .attr('disabled', true);
                    if (this.picked === this.test["answer_true"])
                    {
                        this.answer = true;
                        this.experience++;
                    }
                    this.$http.post("./api/requestApi.php",{
                        method: "getNextSteps",
                        stepId: this.stepId,
                        answer: this.answer,
                        experience: this.experience
                    }).then(function (data) {
                        for (let nextStep in data.data)
                        {
                            $("#" + data.data[nextStep].id)
                                .removeClass("btn-danger")
                                .addClass("btn-success")
                                .attr('disabled', false);
                        }
                    });
                }
            },
            getConfig: function () {
                this.$http.post("./api/requestApi.php",{
                    method: "getConfig",
                }).then(function (data) {
                    if (data.data.mode === "true")
                    {
                        this.treeId = data.data.treeId;
                        this.saveConfig();
                    }
                    else
                    {
                        this.trees = data.data;
                        this.$nextTick(function () {
                            $("#config").modal("show");
                        });
                    }
                })
            },
            getTree: function () {
                this.$http.post("./api/requestApi.php",{
                    method: "getTree",
                    treeId: this.treeId,
                }).then(function (data) {
                    this.levels = data.data;
                    return true;
                })
            },
            saveConfig: function () {
                this.getProgress();
                this.getTree();
                this.getSteps();
                this.config = true;
            },
            openPanel: function () {
                let topPanel = $(".top-panel");
                topPanel.hasClass("openPanel") ? topPanel.removeClass("openPanel") : topPanel.addClass("openPanel");
            },
            getProgress: function () {
                this.$http.post("./api/requestApi.php",{
                    method: "getProgress",
                }).then(function (data) {
                    this.experience = data.data;
                })
            }
        },
        watch: {
          picked: function () {
              $("#btnAnswr").removeAttr('disabled');
          }
        },
        mounted(){
            this.getConfig();
        },
        http:{
            emulateHTTP: true,
            emulateJSON: true
        }
    });
});
