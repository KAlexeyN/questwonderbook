<?php
/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 20.04.2019
 * Time: 22:21
 */
?>
<div id="app">
    <div class="container">
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <div class="col-md-6 col-md-offset-3" v-show="config">
                    <div class="top-panel">
                        <a href="" class="btn-specifications" @click.prevent="openPanel">Информация </a>
                        <div>
                            <div class="message">
                                <p class="auth-quest">Вы авторизованы <a href="./api/logout.php" class="link-quest">Выйти</a></p>
                                <p><a href="./api/clearQuest.php"  class="link-quest">Сбросить прогресс и настройки</a></p>
                                <p>Очки прогресса: {{ experience }}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 offset-md-3">
                    <div class="modal fade" id="config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog modal-md" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel">Настройки</h4>
                                </div>
                                <div class="modal-body">
                                    <span>Выберите дерево шагов:</span>
                                    <select v-model="treeId">
                                        <option v-for="tree in trees">{{ tree.treeId }}</option>
                                    </select>
                                </div>
                                <div class="modal-footer">
                                    <button type="button"  id="btnConfig" class="btn btn-primary" data-toggle="modal" data-target="#config" @click.prevent="saveConfig">Сохранить</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div v-if="Object.keys(steps).length > 0 && config"  v-show="config">
                    <div :id="level.divId" style="text-align: center;" v-for="level in levels">
                        <button type="button" :id="step.id" class="btn btn-circle"
                                :class="[!step.available ? 'btn-secondary': ((steps[step.false_next_steps] && !steps[step.false_next_steps].available && steps[step.false_next_steps].mode === 'wrong') || (steps[step.true_next_steps] && !steps[step.true_next_steps].available && !steps[step.true_next_steps].available && steps[step.true_next_steps].mode === 'right') ? 'btn-success' : (step.false_next_steps || step.true_next_steps? 'btn-danger' : (step.stepType == 'experience'? 'button-orange orange' : 'btn-success'))), 'btn-' + level.divId + level.treeId]"
                                v-for="(step, index) in steps"  v-if="level.indexEnd == 0? step.index == level.indexStart : step.index >= level.indexStart && step.index <= level.indexEnd"
                                :disabled ="!step.available ? true : ((steps[step.false_next_steps] && !steps[step.false_next_steps].available && steps[step.false_next_steps].mode === 'wrong') || (steps[step.true_next_steps] && !steps[step.true_next_steps].available && steps[step.true_next_steps].mode === 'right')? false : (step.false_next_steps || step.true_next_steps || (step.stepType === 'experience' && step.score > experience)? true : false))"
                                data-toggle="modal" data-target="#myModal" @click.prevent="getTest(step.id, step.test_id, step.level_id)">{{ step.step_name }}</button>
                    </div>
                </div>

                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel">{{ test.name }}</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p>{{ test.content }}</p>
                                <p>{{ test.question + '?'}} </p>
                                <div class="radio">
                                    <label><input type="radio" name="optradio" v-model="picked" value="answer1">{{ test.answer1 }}</label>
                                </div>
                                <div class="radio">
                                    <label><input type="radio" name="optradio"  v-model="picked" value="answer2">{{ test.answer2 }}</label>
                                </div>
                                <div class="radio">
                                    <label><input type="radio" name="optradio"  v-model="picked" value="answer3">{{ test.answer3 }}</label>
                                </div>
                                <div class="radio">
                                    <label><input type="radio" name="optradio"  v-model="picked" value="answer4">{{ test.answer4 }}</label>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button"  id="btnAnswr" class="btn btn-primary" data-toggle="modal" data-target="#answer" @click.prevent="sendAnswer" disabled>Ответить</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="answer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-md" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <p v-if="picked === test.answer_true"> {{ test.text_true }} </p>
                                <p v-else> {{ test.text_false }} </p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
