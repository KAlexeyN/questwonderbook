<?php
/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 17.02.2019
 * Time: 14:04
 */
?>
<? require("header.php")?>
<? require_once("auth.php")?>
<? if ($arResult["auth"] && !$arResult["errors"] && $_SESSION["email"] && $_SESSION["password"]):?>
    <? require_once("quest.php")?>
<? else:?>
<div class="container">
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <form class="form" action="" name="formAuth" method="post" novalidate>
                <? if ($arResult["reg"]):?>
                    <h3 class="text-center auth-header">Регистрация</h3>
                    <div class="col-xs-12">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Введите Имя" name="fio" value="<?= $arParams["fio"]? : ""?>"/>
                        </div>
                    </div>
                <? else:?>
                    <h3 class="text-center auth-header">Авторизация</h3>
                <?endif;?>
                <div class="col-xs-12">
                    <div class="form-group">
                        <input type="email" class="form-control" placeholder="Ваш E-mail"  name="email" value="<?= $arParams["email"]? : ""?>"/>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="form-group">
                        <input type="password" class="form-control" placeholder="Ваш пароль" name="password" value="<?= $arParams["password"]? : ""?>"/>
                    </div>
                </div>
                <? if ($arResult["errors"]):?>
                    <ul>
                    <? foreach ($arResult["errors"] as $error):?>
                        <li style="color:red;"><?= $error ?></li>
                    <? endforeach;?>
                    </ul>
                <? endif;?>
                <div class="text-center col-xs-12">
                    <? if ($arResult["reg"]):?>
                        <button type="submit" class="btn btn-from-auth" name="button" value="registrationUser">Зарегистрироваться</button>
                    <? else:?>
                        <button type="submit" class="btn btn-from-auth" name="button" value="authorization">Авторизироваться</button>
                        <button type="submit" class="btn btn-from-auth" name="button" value="registration">Зарегистрироваться</button>
                    <?endif;?>
                </div>
            </form>
        </div>
    </div>
</div>
<? endif;?>
<? require("footer.php")?>
