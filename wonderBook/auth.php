<?php
/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 20.04.2019
 * Time: 22:21
 */

require_once("api/User.php");
$arParams = $_REQUEST;
$auth = new Authorization($arParams);
$arResult = $auth->executive();
class Authorization
{
    private $action = "index";
    private $arParams;
    private $button;
    private $name;
    private $email;
    private $password;
    private $errors = [];

    public function executive()
    {
        switch ($this->action)
        {
            case "index":
                return $this->index();
            default:
                return [];
        }
    }

    public function __construct($arParams)
    {
        if ($arParams)
        {
            $this->arParams = $arParams;
            if ($arParams["action"]) $this->action = $arParams["action"];
            if ($arParams["button"]) $this->button = $arParams["button"];
            if ($arParams["fio"]) $this->name = $arParams["fio"];
            if ($arParams["email"]) $this->email = $arParams["email"];
            if ($arParams["password"]) $this->password = $arParams["password"];
        }
    }

    public function index()
    {
        $arResult = [];
        if (User::isAuthorization())
        {
            $arResult["auth"] = true;
        }
        else
        {
            if ($this->button)
            {
                switch ($this->button)
                {
                    case "registration":
                        $arResult["reg"] = true;
                        break;
                    case "authorization":
                        $this->validateFields();
                        if (!$this->errors)
                        {
                            if (User::authorization($this->email, $this->password))
                            {
                                $arResult["auth"] = true;
                            }
                            else
                            {
                                $this->errors[] = "Вы не зарегистрированы, либо неправильно ввели электронную почту или пароль!";
                            }
                        }
                        break;
                    case "registrationUser":
                        $this->validateFields();
                        if (!$this->errors)
                        {
                            if (User::registration($this->name, $this->email, $this->password))
                            {
                                $arResult["auth"] = true;
                            }
                            else
                            {
                                $arResult["reg"] = true;
                                $this->errors[] = "Такой пользователь уже существует, введите другой email!";
                            }
                        }
                        else
                        {
                            $arResult["reg"] = true;
                        }
                        break;
                    default:
                        break;
                }
            }
            else
            {
                $arResult["auth"] = false;
            }

        }
        if ($this->errors)
        {
            $arResult["errors"] = $this->errors;
        }
        return $arResult;
    }

    public function validateFields()
    {
        if ($this->button === "registrationUser")
        {
            if ($this->name)
            {
                if (strlen($this->name) <= 2) $this->errors[] = "Ваше имя слишком короткое! :) 你好";
            }
            else
            {
                $this->errors[] = "Вы не заполнили своё имя!";
            }
        }
        if (!$this->email) $this->errors[] = "Введите электронную почту!";
        if ($this->password)
        {
            if (strlen($this->password) < 3) $this->errors[] = "Пароль слишком короткий!";
        }
        else
        {
            $this->errors[] = "Введите пароль!";
        }
    }
}
