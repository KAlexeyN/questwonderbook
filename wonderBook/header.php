<?php
/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 17.02.2019
 * Time: 14:04
 */

session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Мой сайт</title>
    <script src="./node/node_modules/vue/dist/vue.js"></script>
    <script src="./node/node_modules/vue-resource/dist/vue-resource.js"></script>
    <script src="./node/node_modules/jquery/dist/jquery.js"></script>
    <script src="./node/node_modules/bootstrap/dist/js/bootstrap.js"></script>
    <link rel="stylesheet" href="./node/node_modules/bootstrap/dist/css/bootstrap.css">
    <link rel="stylesheet" href="./design/style.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
    <script src="./script.js"></script>
</head>
<body>
<div class="bg"></div>