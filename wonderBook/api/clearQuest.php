<?php
/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 19.05.2019
 * Time: 13:08
 */

require_once ("connection.php");
require_once("infoUser.php");
/** @var infoUser $user  */
global $user;
$connection = connectionDB::connection();
$sqlLog = "delete from logs where userId = {$user->getUserId()}";
mysqli_query($connection, $sqlLog);
header('Location: ./../');