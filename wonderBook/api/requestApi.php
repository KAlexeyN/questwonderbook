<?php
/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 23.02.2019
 * Time: 20:46
 */

include_once ($_SERVER["DOCUMENT_ROOT"].'/api/wonderApi.php');

$api = new wonderApi();
$methodName = $_REQUEST["method"];
$result = $api->$methodName();
exit(json_encode($result, JSON_FORCE_OBJECT));
