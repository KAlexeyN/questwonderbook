<?php
/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 23.02.2019
 * Time: 20:22
 */

require_once ("connection.php");
require_once("User.php");
require_once("infoUser.php");
class wonderApi
{
    public function getRequest($request)
    {
        return $_REQUEST[$request];
    }

    public function getSteps()
    {
        $arSteps = [];
        $connection = connectionDB::connection();
        $sql = "SELECT * FROM steps";
        $steps = mysqli_query($connection, $sql);
        $index = 0;
        while ($step = $steps->fetch_assoc())
        {
            $result = $this->getLog("stepId", (string)$step["id"]);
            $step["available"] = $result ? false : true;
            $step["mode"] = $result ? $result["addition"] : "";
            $step["index"] = $index;
            $arSteps[$step["id"]] = $step;
            $index++;
        }
        return $arSteps;

    }

    public function getTest()
    {
        $test_Id = $this->getRequest("testId");
        $level_id = $this->getRequest("levelId");
        $connection = connectionDB::connection();
        $sql = "SELECT * FROM tests WHERE level_id = '$level_id' and id = '$test_Id'";
        return mysqli_query($connection, $sql)->fetch_assoc();
    }

    public function getNextSteps()
    {
        $stepId = $this->getRequest("stepId");
        $answer = $this->getRequest("answer");
        $experience = $this->getRequest("experience");
        $connection = connectionDB::connection();
        $this->getLog("experience") ? $this->updateLog("experience", $experience) : $this->setLog("experience", $experience);

        switch ($answer)
        {
            case "true":
                $sql = "SELECT * FROM steps WHERE true_next_steps = '$stepId'";
                $this->setLog("stepId", $stepId, "right");
                break;
            case "false":
                $sql = "SELECT * FROM steps WHERE false_next_steps = '$stepId'";
                $this->setLog("stepId", $stepId, "wrong");
                break;
            default:
                return [];
        }
        $steps = mysqli_query($connection, $sql);
        $arSteps = [];
        while ($step = $steps->fetch_assoc())
        {

            $arSteps[] = $step;
        }
        return $arSteps;
    }

    public function getConfig()
    {
        /** @var infoUser $user  */
        $connection = connectionDB::connection();
        if ($result = $this->getLog("treeId"))
        {
            return [
                "mode" => "true",
                "treeId" => $result["propertyValue"]
            ];
        }
        else
        {
            $sql = "SELECT treeId FROM tree ORDER BY treeId ASC";
            $treesList = mysqli_query($connection, $sql);
            $trees = [];
            while ($treeId = $treesList->fetch_assoc())
            {
                if (in_array($treeId, $trees)) continue;
                $trees[] = $treeId;
            }
            return $trees;
        }
    }

    public function getTree()
    {
        $treeId = $this->getRequest("treeId");
        $connection = connectionDB::connection();
        $sql = "SELECT * FROM tree WHERE treeId = '$treeId' ORDER BY levelTree ASC";
        $levelsList = mysqli_query($connection, $sql);
        $levels = [];
        while ($level = $levelsList->fetch_assoc())
        {
            $levels[] = $level;
        }
        if (!$this->getLog("treeId"))
        {
            $this->setLog("treeId", $treeId);
        }
        return $levels;
    }

    public function setLog(string $propertyName, string $propertyValue, string $addition = '')
    {
        /** @var infoUser $user  */
        global $user;
        $connection = connectionDB::connection();
        $sqlLog = "INSERT INTO logs (userId, propertyName, propertyValue, addition) values ({$user->getUserId()}, '{$propertyName}', '{$propertyValue}', '{$addition}')";

        mysqli_query($connection, $sqlLog);
    }


    public function getLog(string $propertyName, string $propertyValue = '', string $addition = '') : array
    {
        global $user;
        /** @var infoUser $user  */
        $connection = connectionDB::connection();
        if ($propertyValue)
        {
            $sqlLog = "SELECT * FROM logs WHERE userId = {$user->getUserId()} and propertyName = '{$propertyName}' and propertyValue = '{$propertyValue}'";
        }
        elseif ($propertyValue && $addition)
        {
            $sqlLog = "SELECT * FROM logs WHERE userId = {$user->getUserId()} and propertyName = '{$propertyName}' and propertyValue = '{$propertyValue}' and addition = '{$addition}'";
        }
        else
        {
            $sqlLog = "SELECT * FROM logs WHERE userId = {$user->getUserId()} and propertyName = '{$propertyName}'";
        }
        $resultLog = mysqli_query($connection, $sqlLog)->fetch_assoc();
        return $resultLog ? : [];
    }

    public function updateLog(string $propertyName, string $propertyValue, string $addition = '')
    {
        /** @var infoUser $user  */
        global $user;
        $connection = connectionDB::connection();
        if ($addition)
        {
            $sqlLog = "UPDATE logs SET propertyValue = '{$propertyValue}', addition = '{$addition}' WHERE userId = {$user->getUserId()} and propertyName = '{$propertyName}'";
        }
        else
        {
            $sqlLog = "UPDATE logs SET propertyValue = '{$propertyValue}' WHERE userId = {$user->getUserId()} and propertyName = '{$propertyName}'";
        }
        mysqli_query($connection, $sqlLog);
    }

    public function getProgress()
    {
        $experience = $this->getLog("experience");
        return $experience ? (int)$experience["propertyValue"] : 0;
    }
}