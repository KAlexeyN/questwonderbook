<?php
/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 02.05.2019
 * Time: 13:23
 */

$obj = new Logout();
$obj->exit();
class Logout
{
    public function exit()
    {
        session_start();
        session_unset();
        session_destroy();
        header('Location: ./../');
        exit;
    }
}