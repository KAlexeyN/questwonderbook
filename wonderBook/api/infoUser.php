<?php
/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 03.05.2019
 * Time: 19:26
 */

require_once ("connection.php");
session_start();
if ($_SESSION["email"])
{
    $user = new infoUser($_SESSION["email"]);
    global $user;
}

class infoUser
{
    private $id;
    private $fio;
    private $email;

    public function __construct($email)
    {
        $connection = connectionDB::connection();
        $sql = "SELECT * FROM users WHERE email = '$email'";
        $result = mysqli_query($connection, $sql)->fetch_assoc();
        if ($result)
        {
            $this->id = $result["id"];
            $this->fio = $result["fio"];
            $this->email = $result["email"];
        }
    }

    public function getUserId()
    {
        return (int)$this->id;
    }

    public function getFullName()
    {
        return $this->fio;
    }

    public function getEmail()
    {
        return $this->email;
    }

}